FROM maven:3-openjdk-14-slim AS build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml install

FROM tomcat:jdk14-openjdk-oraclelinux7
COPY --from=build /home/app/target/conference-assistant.war /usr/local/tomcat/webapps/ROOT.war
EXPOSE 8080
